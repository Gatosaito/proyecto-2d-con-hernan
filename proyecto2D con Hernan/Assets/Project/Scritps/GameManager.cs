using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }
    public int PuntosTotales { get { return puntosTotales; } }
    private int puntosTotales;

    public PlayerStadictics playerStadictics;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Debug.Log("Hay mas de un GameManager en la escena");
        }
    }
    public void SumarPuntos(int puntosASumar) 
    {
        puntosTotales += puntosASumar;
    }

    public void Murio() 
    {
        if (playerStadictics.Vidas == 0)
        {
            SceneManager.LoadScene("SampleScene");
        }
    }
}
